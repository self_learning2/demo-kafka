package ru.ovechkin.kafka.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomLogger {

    private CustomLogger() {

    }

    public static void info(final Object msg) {
        log.info(msg.toString());
    }

    public static void error(final Throwable throwable) {
        log.error(throwable.getMessage());
    }

}