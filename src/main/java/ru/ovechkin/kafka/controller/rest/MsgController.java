package ru.ovechkin.kafka.controller.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ovechkin.kafka.dto.UserDto;
import ru.ovechkin.kafka.pojo.Address;
import ru.ovechkin.kafka.util.CustomLogger;

@Slf4j
@RestController
@RequestMapping("send")
public class MsgController {

    @Autowired
    private KafkaTemplate<Long, UserDto> kafkaTemplate;

    @PostMapping("/order")
    public void sendOrder(Long msgId, UserDto msg) {
        msg.setAddress(new Address("RUS", "MSK", "street", 10L, 1L));
        kafkaTemplate.send("msg", msgId, msg);
    }

    @PostMapping("/msg")
    public void sendMsg(Long msgId, UserDto msg) {
        ListenableFuture<SendResult<Long, UserDto>> future = kafkaTemplate.send("msg", msgId, msg);
        future.addCallback(CustomLogger::info, CustomLogger::error);
        kafkaTemplate.flush();
    }

}