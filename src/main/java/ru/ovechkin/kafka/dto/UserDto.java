package ru.ovechkin.kafka.dto;

import lombok.Data;
import ru.ovechkin.kafka.pojo.Address;

@Data
public class UserDto {

    private Long age;

    private String name;

    private Address address;

}