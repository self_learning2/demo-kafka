package ru.ovechkin.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import ru.ovechkin.kafka.dto.UserDto;
import ru.ovechkin.kafka.util.CustomLogger;

@Slf4j
@EnableKafka//Класс, в котором будет создаваться консьюмер необходимо пометить аннотацией
@SpringBootApplication
public class KafkaDemoApplication {

    @KafkaListener(topics="msg")
    public void orderListener(ConsumerRecord<Long, UserDto> record){
        CustomLogger.info(record.partition());
        CustomLogger.info(record.key());
        CustomLogger.info(record.value());
    }

    public static void main(String[] args) {
        SpringApplication.run(KafkaDemoApplication.class, args);
    }

}
